# Contributing

- Feedback, wishes and suggestions can be sent by email.
- Constructive criticism, bug descriptions and other reports are welcome.
- Email: mail@ihub.to

## Sources

- [**GitHub**](https://github.com/cmfstore)
- [GitLab](https://gitlab.com/cmfstore) (MIRROR)
- [Codeberg](https://codeberg.org/cmfstore) (MIRROR)
- [MosHub](https://hub.mos.ru/cmfstore) (MIRROR)
- [Git.Org.Ru](https://git.org.ru/cmfstore) (MIRROR)
