# Information

| Property     | Value                                              |
| ------------ | -------------------------------------------------- |
| ID           | `ext_4b8213a1`                                     |
| Type         | Add-on                                             |
| License      | MIT                                                |
| Language     | Russian                                            |
| Requirements | XenForo 2.1                                        |
| Authors      | [iHub TO](mailto:mail@ihub.to)             |

Дополнительные META-теги для [**XenForo**](https://xenforo.com).

## Install

1. [Загрузить](https://github.com/cmfstore/xenforo-metatag/tags) архив с последней версией расширения.
2. Распаковать содержимое архива в `/src/addons/PkgStore/ext_4b8213a1/`, сохраняя структуру директорий.
3. Зайти в **AdminCP**, далее *Add-ons*, и установить необходимое расширение.

## Update

1. [Загрузить](https://github.com/cmfstore/xenforo-metatag/tags) архив с новой версией расширения.
2. Распаковать содержимое архива в `/src/addons/PkgStore/ext_4b8213a1/`, сохраняя структуру директорий, заменяя существующие файлы и папки.
3. Зайти в **AdminCP**, далее *Add-ons*, и обновить необходимое расширение.
